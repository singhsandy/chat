package com.example.sandeep.chatapplication.view.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.sandeep.chatapplication.R;
import com.example.sandeep.chatapplication.helper.MessageHelper;
import com.example.sandeep.chatapplication.model.Message;
import com.example.sandeep.chatapplication.view.adapter.ChatAdapter;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatFragment extends Fragment implements MessageHelper.OnMessageFetchListener {

    private ChatAdapter chatAdapter;
    private LinearLayoutManager layoutManager;

    @BindView(R.id.rv)
    RecyclerView chatRecyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        super.onCreateView(inflater,container,savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_layout, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        chatAdapter = new ChatAdapter(getActivity());
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,false);
        chatRecyclerView.setLayoutManager(layoutManager);
        chatRecyclerView.setAdapter(chatAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(),
                layoutManager.getOrientation());
        chatRecyclerView.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public void onStart(){
        super.onStart();
        MessageHelper messageHelper = new MessageHelper(getActivity());
        messageHelper.fetchMessages(this);
    }

    @Override
    public void onMessageFetched(ArrayList<Message> messages) {
        chatAdapter.setData(messages);
    }
}
