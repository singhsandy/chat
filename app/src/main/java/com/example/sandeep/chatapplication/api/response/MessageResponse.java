package com.example.sandeep.chatapplication.api.response;

import com.example.sandeep.chatapplication.model.Message;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class MessageResponse implements Serializable {
    @SerializedName("count")
    private int count;

    @SerializedName("messages")
    private ArrayList<Message> messages;

    public MessageResponse(int count, ArrayList<Message> messages) {
        this.count = count;
        this.messages = messages;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }
}
