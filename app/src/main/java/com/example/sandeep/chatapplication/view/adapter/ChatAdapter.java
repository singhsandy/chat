package com.example.sandeep.chatapplication.view.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.example.sandeep.chatapplication.R;
import com.example.sandeep.chatapplication.data.DAO;
import com.example.sandeep.chatapplication.model.Message;
import com.example.sandeep.chatapplication.view.fragment.UserFragment;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChatAdapter extends RecyclerView.Adapter {

    private ArrayList<Message> messages = new ArrayList<>();

    private Context mContext;
    public ChatAdapter(Context context){
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ChatHolder(LayoutInflater.from(mContext).inflate(R.layout.chat_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Message message = messages.get(position);
        ChatHolder chatHolder = (ChatHolder) holder;
        chatHolder.senderTxt.setText(message.getName());
        chatHolder.messageTxt.setText(message.getBody());
        chatHolder.profileImage.setImageURI(message.getImageUrl(), this);
        chatHolder.favoriteBtn.setChecked(message.isFavourite());
        chatHolder.favoriteBtn.setButtonDrawable(message.isFavourite()? AppCompatResources.
                getDrawable(mContext, R.drawable.wishlist_icon_selected):
                AppCompatResources.getDrawable(mContext, R.drawable.wishlist_icon));

    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public void setData(ArrayList<Message> messages) {
        this.messages.clear();
        this.messages = messages;
        notifyDataSetChanged();
    }

    class ChatHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.image)
        SimpleDraweeView profileImage;

        @BindView(R.id.senderTxt)
        TextView senderTxt;

        @BindView(R.id.messageTxt)
        TextView messageTxt;

        @BindView(R.id.toggle)
        ToggleButton favoriteBtn;

        @OnClick(R.id.toggle)
        public void onClick(){
            messages.get(getAdapterPosition()).setFavourite(favoriteBtn.isChecked());
            notifyItemChanged(getAdapterPosition());
            DAO dao = new DAO(itemView.getContext());
            dao.markFavourite(messages.get(getAdapterPosition()));
            Intent intent = new Intent();
            intent.setAction(UserFragment.FAVOURITE_INTENT_FILTER);
            mContext.sendBroadcast(intent);
        }

        public ChatHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
