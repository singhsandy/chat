package com.example.sandeep.chatapplication.helper;

import android.content.Context;
import android.content.ContextWrapper;
import android.widget.Toast;

import com.example.sandeep.chatapplication.api.RestClient;
import com.example.sandeep.chatapplication.api.response.MessageResponse;
import com.example.sandeep.chatapplication.data.DAO;
import com.example.sandeep.chatapplication.model.Message;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageHelper extends ContextWrapper {

    private OnMessageFetchListener onMessageFetchListener;

    public MessageHelper(Context base) {
        super(base);
    }

    public void fetchMessages(OnMessageFetchListener onMessageFetchListener) {
        this.onMessageFetchListener = onMessageFetchListener;
        try {
            DAO chatDB = new DAO(getApplicationContext());
            if (chatDB.getMessages().size()>0)
                this.onMessageFetchListener.onMessageFetched(chatDB.getMessages());
            else
                fetchMessagesFromServer();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fetchMessagesFromServer() {
        RestClient.getInstance(getApplicationContext()).getApiService().getChats().enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                if (response!=null && response.body()!=null && response.code()==200) {
                    ArrayList<Message> messages = response.body().getMessages();
                    try {
                        DAO chatDb = new DAO(getApplicationContext());
                        for (Message message: messages)
                            chatDb.insertMessage(message);
                        onMessageFetchListener.onMessageFetched(chatDb.getMessages());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                Toast.makeText(getApplicationContext(), "Some Error Occured", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Some Error Occured", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public interface OnMessageFetchListener {
        void onMessageFetched(ArrayList<Message> messages);
    }
}
