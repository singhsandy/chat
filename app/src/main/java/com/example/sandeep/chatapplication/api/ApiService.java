package com.example.sandeep.chatapplication.api;

import com.example.sandeep.chatapplication.api.response.MessageResponse;

import retrofit2.Call;
import retrofit2.http.GET;


public interface ApiService {

    @GET("test_data")
    Call<MessageResponse> getChats();
}

