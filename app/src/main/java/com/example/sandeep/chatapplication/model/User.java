package com.example.sandeep.chatapplication.model;


public class User {

    private String userName;
    private String name;
    private int messageCount;
    private int favoriteCount;
    private String profileImage;

    public User(String userName, String name,int messageCount, int favoriteCount, String profileImage) {
        this.userName = userName;
        this.name = name;
        this.messageCount = messageCount;
        this.favoriteCount = favoriteCount;
        this.profileImage = profileImage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getMessageCount() {
        return messageCount;
    }

    public String getName() {
        return name;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMessageCount(int messageCount) {
        this.messageCount = messageCount;
    }

    public int getFavoriteCount() {
        return favoriteCount;
    }

    public void setFavoriteCount(int favoriteCount) {
        this.favoriteCount = favoriteCount;
    }
}
