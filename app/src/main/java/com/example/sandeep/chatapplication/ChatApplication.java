package com.example.sandeep.chatapplication;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

public class ChatApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
    }
}
