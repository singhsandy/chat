package com.example.sandeep.chatapplication.view.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sandeep.chatapplication.R;
import com.example.sandeep.chatapplication.data.DAO;
import com.example.sandeep.chatapplication.helper.MessageHelper;
import com.example.sandeep.chatapplication.model.User;
import com.example.sandeep.chatapplication.view.adapter.UserAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserFragment extends Fragment {

    @BindView(R.id.rv)
    RecyclerView userRecyclerView;

    private UserAdapter userAdapter;
    private LinearLayoutManager layoutManager;
    private DAO mDao;
    public static final String FAVOURITE_INTENT_FILTER = "favourite_intent_filter";


    private BroadcastReceiver favouriteChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (userAdapter!=null && mDao != null)
                userAdapter.setData(mDao.getUsers());
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_layout, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userAdapter = new UserAdapter(getActivity());
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        userRecyclerView.setLayoutManager(layoutManager);
        userRecyclerView.setAdapter(userAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(),
                layoutManager.getOrientation());
        userRecyclerView.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public void onStart(){
        super.onStart();
        getActivity().registerReceiver(favouriteChangeReceiver, new IntentFilter(FAVOURITE_INTENT_FILTER));

    }

    @Override
    public void onResume(){
        super.onResume();
        mDao = new DAO(getContext());
        ArrayList<User> users = mDao.getUsers();
        userAdapter.setData(users);
    }

    @Override
    public void onStop(){
        getActivity().unregisterReceiver(favouriteChangeReceiver);
        super.onStop();
    }

}
