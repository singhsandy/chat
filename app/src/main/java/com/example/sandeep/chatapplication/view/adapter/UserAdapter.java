package com.example.sandeep.chatapplication.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sandeep.chatapplication.R;
import com.example.sandeep.chatapplication.model.User;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sandeep on 2/20/2017.
 */

public class UserAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private ArrayList<User> userList = new ArrayList<>();

    public UserAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.user_item, parent, false);
        return new UserHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        User user = userList.get(position);
        UserHolder userHolder = (UserHolder) holder;
        userHolder.nameTxt.setText(user.getName());
        userHolder.userNameTxt.setText(user.getUserName());
        userHolder.profileImage.setImageURI(user.getProfileImage(), mContext);
        userHolder.totalMsgCount.setText(user.getMessageCount()+"");
        userHolder.favouriteCountTv.setText(user.getFavoriteCount()+"");
    }


    public void setData(ArrayList<User> users) {
        this.userList.clear();
        this.userList = users;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class UserHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        SimpleDraweeView profileImage;

        @BindView(R.id.nameTv)
        TextView nameTxt;

        @BindView(R.id.usernameTv)
        TextView userNameTxt;

        @BindView(R.id.totalMsgCountTv)
        TextView totalMsgCount;

        @BindView(R.id.favouriteCountTv)
        TextView favouriteCountTv;

        public UserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
