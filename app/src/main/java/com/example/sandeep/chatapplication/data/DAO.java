package com.example.sandeep.chatapplication.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.sandeep.chatapplication.model.Message;
import com.example.sandeep.chatapplication.model.User;

import java.util.ArrayList;

public class DAO {
    private DBSchema mHelper;
    public Context mContext;

    public DAO(Context context){
        this.mContext = context;
        mHelper = new DBSchema(mContext);
    }

    private SQLiteDatabase getReadDB(){
        return mHelper.getReadableDatabase();
    }

    private SQLiteDatabase getWriteDB(){
        return mHelper.getWritableDatabase();
    }

    public void insertMessage(Message message) {
        SQLiteDatabase db = getWriteDB();
        ContentValues values = new ContentValues();
        values.put(DBSchema.TB_CHAT.BODY, message.getBody());
        values.put(DBSchema.TB_CHAT.MESSAGE_TIME, message.getTimeStamp());
        values.put(DBSchema.TB_CHAT.USERNAME, message.getUserName());
        values.put(DBSchema.TB_CHAT.NAME, message.getName());
        values.put(DBSchema.TB_CHAT.FAVORITE_MESSAGE, message.isFavourite()?1:0);
        values.put(DBSchema.TB_CHAT.IMAGE_URL, message.getImageUrl());
        db.insert(DBSchema.TABLE_CHAT, null, values);
        db.close();
    }

    public void insertMessages(ArrayList<Message> messages){
        SQLiteDatabase db = getWriteDB();
        db.beginTransaction();
        try{
            ContentValues values = new ContentValues();
            for(Message message: messages){
                values.put(DBSchema.TB_CHAT.BODY, message.getBody());
                values.put(DBSchema.TB_CHAT.MESSAGE_TIME, message.getTimeStamp());
                values.put(DBSchema.TB_CHAT.USERNAME, message.getUserName());
                values.put(DBSchema.TB_CHAT.NAME, message.getName());
                values.put(DBSchema.TB_CHAT.FAVORITE_MESSAGE, message.isFavourite()?1:0);
                values.put(DBSchema.TB_CHAT.IMAGE_URL, message.getImageUrl());
                db.insert(DBSchema.TABLE_CHAT, null, values);
            }
            db.setTransactionSuccessful();
        }finally {
            db.endTransaction();
        }
    }

    public ArrayList<Message> getMessages() {
        SQLiteDatabase db = getReadDB();
        Cursor c = db.rawQuery("SELECT * FROM " + DBSchema.TABLE_CHAT +
                " ORDER BY " + DBSchema.TB_CHAT.MESSAGE_TIME, null);
        ArrayList<Message> messages = new ArrayList<>();
        if (c.getCount()==0) {
            c.close();
            return messages;
        }
        while (c.moveToNext()) {
            int id = c.getInt(c.getColumnIndex(DBSchema.TB_CHAT.ID));
            String timestamp = c.getString(c.getColumnIndex(DBSchema.TB_CHAT.MESSAGE_TIME));
            String body = c.getString(c.getColumnIndex(DBSchema.TB_CHAT.BODY));
            String user = c.getString(c.getColumnIndex(DBSchema.TB_CHAT.USERNAME));
            String name = c.getString(c.getColumnIndex(DBSchema.TB_CHAT.NAME));
            String image = c.getString(c.getColumnIndex(DBSchema.TB_CHAT.IMAGE_URL));
            boolean isFavorite = c.getInt(c.getColumnIndex(DBSchema.TB_CHAT.FAVORITE_MESSAGE))==1;
            Message message = new Message(id, body, user, image, timestamp, name, isFavorite);
            messages.add(message);
        }
        c.close();
        db.close();
        return messages;
    }

    public boolean markFavourite(Message message) {
        SQLiteDatabase db  = getWriteDB();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBSchema.TB_CHAT.FAVORITE_MESSAGE, message.isFavourite()?1:0);
        int result = db.update(DBSchema.TABLE_CHAT, contentValues,
                DBSchema.TB_CHAT.ID + "=?", new String[]{message.getId() + ""});
        db.close();
        return result != -1;
    }

    public ArrayList<User> getUsers() {
        SQLiteDatabase db = getReadDB();
        Cursor c = db.rawQuery("SELECT *, count(*), sum(" + DBSchema.TB_CHAT.FAVORITE_MESSAGE + ") FROM "
                + DBSchema.TABLE_CHAT + " GROUP BY " + DBSchema.TB_CHAT.USERNAME, null);
        ArrayList<User> users = new ArrayList<>();
        if (c.getCount()==0) {
            c.close();
            return users;
        }
        while (c.moveToNext()){
            String senderName = c.getString(c.getColumnIndex(DBSchema.TB_CHAT.NAME));
            String senderUserName = c.getString(c.getColumnIndex(DBSchema.TB_CHAT.USERNAME));
            String userProfile = c.getString(c.getColumnIndex(DBSchema.TB_CHAT.IMAGE_URL));
            int messageCount = c.getInt(7);
            int favoriteCount = c.getInt(8);
            User user = new User(senderUserName, senderName, messageCount, favoriteCount,userProfile);
            users.add(user);
        }
        return users;
    }

}
