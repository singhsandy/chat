package com.example.sandeep.chatapplication.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import retrofit2.http.PUT;

/**
 * Created by sandeep on 2/20/2017.
 */

public class DBSchema extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "chat.db";

    public DBSchema(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static final String TABLE_CHAT = "chat";
    private static final String COMMA_SPACE     = ", ";
    private static final String CREATE_TABLE    = "CREATE TABLE ";
    private static final String PRIMARY_KEY     = "PRIMARY KEY ";
    private static final String TYPE_TEXT       = " TEXT ";
    private static final String TYPE_INT        = " INTEGER ";
    private static final String NOT_NULL        = "NOT NULL ";


    public static final class TB_CHAT{
        public static final String ID = "_id";
        public static final String BODY = "body";
        public static final String USERNAME = "username";
        public static final String NAME = "Name";
        public static final String IMAGE_URL = "image_url";
        public static final String MESSAGE_TIME = "message_time";
        public static final String FAVORITE_MESSAGE = "favorite_message";
    }

    private static final String CREATE_TABLE_CHAT =
            CREATE_TABLE + TABLE_CHAT + " ( " +
                    TB_CHAT.ID + TYPE_INT +NOT_NULL + PRIMARY_KEY + COMMA_SPACE +
                    TB_CHAT.BODY + TYPE_TEXT + COMMA_SPACE +
                    TB_CHAT.USERNAME + TYPE_TEXT + COMMA_SPACE +
                    TB_CHAT.NAME + TYPE_TEXT + COMMA_SPACE +
                    TB_CHAT.IMAGE_URL + TYPE_TEXT + COMMA_SPACE +
                    TB_CHAT.MESSAGE_TIME + TYPE_TEXT +  COMMA_SPACE +
                    TB_CHAT.FAVORITE_MESSAGE + TYPE_INT + "DEFAULT 0" +
                    ") ";
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_CHAT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }
}
